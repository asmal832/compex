<?php

class Complex
{
    private $real;
    private $imag;

    public function __construct(float $r, float $i)
    {
        $this->real = $r;
        $this->imag = $i;
    }

    public function getReal()
    {
        return $this->real;
    }

    public function getImag()
    {
        return $this->imag;
    }

    public static function plus(self $a, self $b)
    {
        $real = $a->getReal() + $b->getReal();
        $imag = $a->getImag() + $b->getImag();

        return new self($real, $imag);
    }

    public static function minus(self $a, self $b)
    {
        $real = $a->getReal() - $b->getReal();
        $imag = $a->getImag() - $b->getImag();

        return new self($real, $imag);
    }

    public static function multiply(self $a, self $b)
    {
        $a_r = $a->getReal();
        $a_i = $a->getImag();

        $b_r = $b->getReal();
        $b_i = $b->getImag();

        $real = $a_r * $b_r - $a_i * $b_i;
        $imag = $b_r * $a_i + $a_r * $b_i;

        return new self($real, $imag);
    }

    public static function divide(self $a, self $b)
    {
        $a_r = $a->getReal();
        $a_i = $a->getImag();

        $b_r = $b->getReal();
        $b_i = $b->getImag();

        $real = ($a_r * $b_r + $a_i * $b_i) / ($b_r ** 2 + $b_i ** 2);
        $imag = ($b_r * $a_i - $a_r * $b_i) / ($b_r ** 2 + $b_i ** 2);

        return new self($real, $imag);
    }
}

