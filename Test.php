<?php


class Test
{
    private $metods;
    private $class_name;

    public function __construct($class_name)
    {
        require_once $class_name . ".php";

        $this->class_name = $class_name;
        $this->metods = get_class_methods($class_name);
    }

    private function ok($metodName)
    {
        echo("OK CLASS: {$this->class_name}, METOD: {$metodName} \r\n");
    }

    private function error($metodName)
    {
        echo("ERROR CLASS: {$this->class_name}, METOD: {$metodName} \r\n");
    }

    public function run()
    {
        $a_r = 2;
        $a_i = 3;

        $b_r = 4;
        $b_i = 5;

        try {

            $a = new Complex($a_r, $a_i);
            $b = new Complex($b_r, $b_i);

            foreach ($this->metods as $metodName) {

                switch ($metodName) {
                    case '__construct':
                    case 'getReal':
                    case 'getImag':
                        $a->getReal() == $a_r && $a->getImag() == $a_i ? $this->ok($metodName) : $this->error($metodName);
                        break;
                    case 'plus':
                        $res = Complex::plus($a, $b);
                        $res->getReal() == 6 && $res->getImag() == 8 ? $this->ok($metodName) : $this->error($metodName);
                        break;
                    case 'minus':
                        $res = Complex::minus($a, $b);
                        $res->getReal() == -2 && $res->getImag() == -2 ? $this->ok($metodName) : $this->error($metodName);
                        break;
                    case 'multiply':
                        $res = Complex::multiply($a, $b);
                        $res->getReal() == -7 && $res->getImag() == 22 ? $this->ok($metodName) : $this->error($metodName);
                        break;
                    case 'divide':
                        $res = Complex::divide($a, $b);
                        round($res->getReal(), 14) == 0.5609756097561
                        &&
                        round($res->getImag(), 14) == 0.04878048780488 ? $this->ok($metodName) : $this->error($metodName);
                        break;
                    default:
                        echo("ERROR CLASS: {$this->class_name}, METOD: {$metodName} test not found \r\n");

                }
            }
        } catch (Exception $e) {
            echo("ERROR CLASS: {$this->class_name}\r\n");
        }
    }
}

$test = new Test("Complex");
$test->run();
